export const environment = {
  production: true,
  apiUrl: 'http://192.168.63.67:30061',
  pathPrefixLookup: `:40013/lookup`,
  pathPrefixNurse:`:40012/nurse`,
  pathPrefixAuth: `:40010/auth`
};
