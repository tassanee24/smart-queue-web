export const environment = {
  production: false,
  apiUrl: 'http://192.168.63.67:30061',
  pathPrefixLookup: `:40013/lookup`,
  pathPrefixNurse:`http://192.168.63.67:3000/nurse`,
  pathPrefixAuth: `:40010/auth`
};
