import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QueueErComponent } from './queue-er.component';

const routes: Routes = [{ path: '', component: QueueErComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QueueErRoutingModule { }
