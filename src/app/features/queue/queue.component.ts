import { Component } from '@angular/core';
// import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NzButtonSize } from 'ng-zorro-antd/button';
import { QueueService } from './services/queue.service'
import { DateTime } from 'luxon';
import { Subscription, interval } from 'rxjs';


@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.css'],

})
export class QueueComponent {
  private updateSubscriptionVisit!: Subscription;
  private updateSubscriptionDateTime!: Subscription;
  NzDemoGridBasicComponent: any
  dataSet: any;
  RST0ER: any;
  RST1ER: any;
  ertotal: any;
  RST0OPD: any;
  opdtotal: any;
  RST0DENT: any;
  RST1DENT: any;
  denttotal: any;
  date: any;
  time: any;
  ipdvisit: any;
  ptappointAll: any;
  ptappointRegister: any;
  ptappointMissRegister: any;
  opdvisittotal: any;
  visitresultScreen: any;
  visitresultSendDoctor: any;
  visitresultSuccess: any;
  visitDrugWait: any;
  visitDrugSuccess: any;
  visitWaitDoctor: any;

  constructor(
    private queueService: QueueService,
  ) { }

  ngOnInit(): void {
    this.getVisit();
    this.updateSubscriptionVisit = interval(30000).subscribe(
      (val) => { this.getVisit() });

    this.updateSubscriptionDateTime = interval(1000).subscribe(
      (val) => { this.getDateTime() });
  }

  submitForm(): void {

  }

  async getVisit() {
    try {
      const response = await this.queueService.getVisit();
      this.dataSet = response.data;
      console.log(this.dataSet);

      if (this.dataSet.data.length > 0) {
        //Begin Visit ER
        this.RST0ER = this.dataSet.data.filter((item: { result: string }) => {
          return item.result === `RST0ER`;
        });

        this.RST1ER = this.dataSet.data.filter((item: { result: string }) => {
          return item.result === `RST1ER`;
        });

        this.ertotal = this.dataSet.ertotal;

        // End Visit ER

        // Begin Visit OPD
        this.RST0OPD = this.dataSet.data.filter((item: { result: string }) => {
          return item.result === `RST0OPD`;
        });

        this.opdtotal = this.dataSet.opdtotal;
        this.opdvisittotal = this.dataSet.opdvisittotal;
        this.ptappointAll = this.dataSet.ptappoint;
        this.ptappointRegister = this.dataSet.ptappointregister;
        this.ptappointMissRegister = this.dataSet.ptappoint[0].total - this.dataSet.ptappointregister[0].total;

        this.visitresultScreen = this.dataSet.visitresult.filter((item: { name: string }) => {
          return item.name === `ส่งคัดกรอง`
        });

        this.visitresultSendDoctor = this.dataSet.visitresult.filter((item: { name: string }) => {
          return item.name === `ส่งตรวจโรค`
        });

        this.visitWaitDoctor = this.dataSet.visitresult.filter((item: { name: string }) => {
          return item.name === `รอตรวจ`
        });

        this.visitresultSuccess = this.dataSet.visitresult.filter((item: { name: string }) => {
          return item.name === `ส่งรับยา`
        });

        this.visitDrugWait = this.dataSet.drugvisit.filter((item: { drugtype: string }) => {
          return item.drugtype === `รอรับยา`
        });

        this.visitDrugSuccess = this.dataSet.drugvisit.filter((item: { drugtype: string }) => {
          return item.drugtype === `รับยากลับบ้าน`
        });

        //End Visit OPD

        //Begin Visit IPD
        this.ipdvisit = this.dataSet.ipdvisit;
        //End Visit IPD

        // Begin Visit DENT
        this.RST0DENT = this.dataSet.data.filter((item: { result: string }) => {
          return item.result === `RST0DENT`;
        });

        this.RST1DENT = this.dataSet.data.filter((item: { result: string }) => {
          return item.result === `RST1DENT`;
        });

        this.denttotal = this.dataSet.denttotal;

        //End Visit DENT
      }
    } catch (error: any) {

    }

  }

  async getDateTime() {
    this.date = DateTime.now().setLocale('th').toLocaleString(DateTime.DATE_FULL);
    this.time = DateTime.now().setLocale('th').toLocaleString(DateTime.TIME_24_WITH_SECONDS);
    // console.log(this.date);
  }

}
