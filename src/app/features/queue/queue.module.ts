import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QueueRoutingModule } from './queue-routing.module';
import { QueueComponent } from './queue.component';
import { NgZorroModule } from '../../ng-zorro.module';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    QueueComponent
  ],
  imports: [
    CommonModule,
    QueueRoutingModule,
    NgZorroModule,
    SharedModule,
  ]
})
export class QueueModule { }
