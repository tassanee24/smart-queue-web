import { Injectable } from '@angular/core';

import { NzNotificationPlacement, NzNotificationService } from 'ng-zorro-antd/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private notification: NzNotificationService) { }

  notificationSuccess(title: any, content: any, position: NzNotificationPlacement): void {
    this.notification.success(
      title, content,
      {
        nzPlacement: position,
        nzAnimate: true,
        nzCloseIcon: 'close-circle',
        nzDuration:1500

      }
    );
  }
  notificationInfo(title: any, content: any, position: NzNotificationPlacement): void {
    this.notification.info(
      title, content,
      {
        nzPlacement: position,
        nzAnimate: true,
        nzCloseIcon: 'close-circle',
        nzDuration:1500

      }
    );
  }
  notificationWaring(title: any, content: any, position: NzNotificationPlacement): void {
    this.notification.warning(
      title, content,
      {
        nzPlacement: position,
        nzAnimate: true,
        nzCloseIcon: 'close-circle',
        nzDuration:1500

      }
    );
  }
  notificationError(title: any, content: any, position: NzNotificationPlacement): void {
    this.notification.error(
      title, content,
      {
        nzPlacement: position,
        nzAnimate: true,
        nzCloseIcon: 'close-circle',
        nzDuration:0

      }
    );
  }

  

}
